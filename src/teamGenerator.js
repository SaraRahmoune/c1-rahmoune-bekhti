class TeamGenerator {
  constructor(players, playersPerTeam = 3) {
    if (!players || players.length === 0) {
      throw new Error('Le tableau de joueurs ne peut pas être vide.');
    }
    if (playersPerTeam < 1) {
      throw new Error('Le nombre de joueurs par équipe doit être positif.');
    }
    if (playersPerTeam > players.length) {
      throw new Error('Le nombre de joueurs par équipe ne peut pas être supérieur au nombre de joueurs.');
    }

    this.players = players;
    this.playersPerTeam = playersPerTeam;
    this.teams = [];
  }

  generateTeams() {
    let shuffledPlayers = [...this.players].sort(() => 0.5 - Math.random());
    let teamIndex = 0;

    while (shuffledPlayers.length > 0) {
      let teamPlayers = shuffledPlayers.splice(0, this.playersPerTeam);
      let teamName = `Équipe ${teamIndex + 1}`;
      let team = {
        name: teamName,
        players: teamPlayers,
      };
      this.teams.push(team);
      teamIndex++;
    }

    return this.teams;
  }

  addPlayer(player) {
    this.players.push(player);
  }

  removePlayer(player) {
    const index = this.players.indexOf(player);
    if (index > -1) {
      this.players.splice(index, 1);
    }
  }

  getTeams() {
    return this.teams;
  }
}

export default TeamGenerator;