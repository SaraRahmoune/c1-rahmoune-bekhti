import { expect } from 'chai';
import TournamentGenerator from '../src/tournamentGenerator.js';

describe('TournamentGenerator', () => {
    it('La création de TournamentGenerator est correctement effectuée', () => {
        const teams = [
            { name: 'Team 1', players: ['Player 1', 'Player 2', 'Player 3'] },
            { name: 'Team 2', players: ['Player 4', 'Player 5', 'Player 6'] },
            { name: 'Team 3', players: ['Player 7', 'Player 8', 'Player 9'] },
            { name: 'Team 4', players: ['Player 10', 'Player 11', 'Player 12'] },
        ];

        const tournamentGenerator = new TournamentGenerator(teams);

        expect(tournamentGenerator).to.be.an.instanceof(TournamentGenerator);
        expect(tournamentGenerator.teams).to.deep.equal(teams);
    });

    it('should throw an error when initialized with an empty teams array', () => {
        const teams = [];
        expect(() => new TournamentGenerator(teams)).to.throw(Error, 'Teams array cannot be empty');
    });

    it('should generate the correct number of poules based on the number of teams', () => {
        const teams = [
            { name: 'Team 1', players: ['Player 1', 'Player 2', 'Player 3'] },
            { name: 'Team 2', players: ['Player 4', 'Player 5', 'Player 6'] },
            { name: 'Team 3', players: ['Player 7', 'Player 8', 'Player 9'] },
            { name: 'Team 4', players: ['Player 10', 'Player 11', 'Player 12'] },
            { name: 'Team 5', players: ['Player 13', 'Player 14', 'Player 15'] },
            { name: 'Team 6', players: ['Player 16', 'Player 17', 'Player 18'] },
            { name: 'Team 7', players: ['Player 19', 'Player 20', 'Player 21'] },
            { name: 'Team 8', players: ['Player 22', 'Player 23', 'Player 24'] },
        ];
        const tournamentGenerator = new TournamentGenerator(teams);

        tournamentGenerator.generatePoules();

        expect(tournamentGenerator.poules).to.have.lengthOf(teams.length / 4);
    });

    it('should correctly select teams for the final phase based on the results of the poule matches', () => {
        const teams = [
            { name: 'Team 1', players: ['Player 1', 'Player 2', 'Player 3'] },
            { name: 'Team 2', players: ['Player 4', 'Player 5', 'Player 6'] },
            { name: 'Team 3', players: ['Player 7', 'Player 8', 'Player 9'] },
            { name: 'Team 4', players: ['Player 10', 'Player 11', 'Player 12'] },
            { name: 'Team 5', players: ['Player 13', 'Player 14', 'Player 15'] },
            { name: 'Team 6', players: ['Player 16', 'Player 17', 'Player 18'] },
            { name: 'Team 7', players: ['Player 19', 'Player 20', 'Player 21'] },
            { name: 'Team 8', players: ['Player 22', 'Player 23', 'Player 24'] },
        ];
        const tournamentGenerator = new TournamentGenerator(teams);
        tournamentGenerator.generatePoules();
        tournamentGenerator.simulatePoulesMatches();
        expect(tournamentGenerator.finalStages[0]).to.have.lengthOf(teams.length / 2);
    });
});