import { expect } from 'chai';
import TeamGenerator from '../src/teamGenerator.js';

describe('TeamGenerator', () => {
    describe('Test de création de tournoi avec les paramètres de base', () => {
        it('Vérifier si la fonction de création de tournoi crée effectivement un tournoi avec les paramètres spécifiés (nom, type, participants)', () => {
            // Arrange
            const players = ['Player 1', 'Player 2', 'Player 3'];
            const playersPerTeam = 3;

            
            const teamGenerator = new TeamGenerator(players, playersPerTeam);
            const tournament = teamGenerator.generateTeams();

           
            expect(tournament).to.not.be.undefined;
            expect(tournament[0].name).to.equal('Équipe 1');
            players.forEach(player => {
                expect(tournament[0].players).to.include(player);
            });
        });

        it('Vérifier si les données du tournoi créé correspondent aux valeurs attendues', () => {
            
            const players = ['Player 1', 'Player 2', 'Player 3', 'Player 4', 'Player 5', 'Player 6'];
            const playersPerTeam = 3;

            
            const teamGenerator = new TeamGenerator(players, playersPerTeam);
            const tournament = teamGenerator.generateTeams();

            
            expect(tournament.length).to.equal(2);
            expect(tournament[0].players.length).to.equal(playersPerTeam);
            expect(tournament[1].players.length).to.equal(playersPerTeam);
        });
    });

    describe('Test de validation des entrées', () => {
        it('Doit lancer une erreur si le tableau de joueurs est vide', () => {
            expect(() => new TeamGenerator([])).to.throw('Le tableau de joueurs ne peut pas être vide.');
        });

        it('Doit lancer une erreur si le nombre de joueurs par équipe est négatif', () => {
            const players = ['Player 1', 'Player 2', 'Player 3'];
            expect(() => new TeamGenerator(players, -1)).to.throw('Le nombre de joueurs par équipe doit être positif.');
        });

        it('Doit lancer une erreur si le nombre de joueurs par équipe est supérieur au nombre de joueurs', () => {
            const players = ['Player 1', 'Player 2', 'Player 3'];
            expect(() => new TeamGenerator(players, 4)).to.throw('Le nombre de joueurs par équipe ne peut pas être supérieur au nombre de joueurs.');
        });
    });

    describe('Test de gestion des participants', () => {
        it('Doit ajouter un participant à un tournoi existant et mettre à jour la liste des participants correctement', () => {
           
            const players = ['Player 1', 'Player 2', 'Player 3'];
            const teamGenerator = new TeamGenerator(players);
            const newPlayer = 'Player 4';

            
            teamGenerator.addPlayer(newPlayer);

           
            expect(teamGenerator.players).to.include(newPlayer);
        });

        it('Doit supprimer un participant d\'un tournoi et mettre à jour la liste des participants correctement', () => {
           
            const players = ['Player 1', 'Player 2', 'Player 3'];
            const teamGenerator = new TeamGenerator(players);
            const playerToRemove = 'Player 2';

            
            teamGenerator.removePlayer(playerToRemove);

            
            expect(teamGenerator.players).to.not.include(playerToRemove);
        });
    });
});